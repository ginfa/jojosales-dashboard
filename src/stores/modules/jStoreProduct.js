/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiProduct from '@/services/jApiProduct';
import apiMeasure from '@/services/jApiMeasure';

const state = {
  // Set default state(s) here
  product_state: {
    measure_list: [],
    product_detail: null,
  },
  data_changed: false,
};

const getters = {
  measure_list() {
    // const listData = state.product_state.measure_list
  },
};

const mutations = {
  setProductDetail(state, payload) {
    state.product_state.product_detail = payload.data;
  },
  setMeasureOptions(state, payload) {
    const Measure = payload.data.map((item) => {
      const option = {
        id: item.id,
        name: item.name,
      };
      return option;
    });

    state.product_state.measure_list = Measure;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getMeasureOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getMeasureList = await apiMeasure.getMeasureList();
      commit('setMeasureOptions', getMeasureList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getProductDetail({ commit, dispatch }, reqId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getDetail = await apiProduct.getProductDetail(reqId);
      commit('setProductDetail', getDetail);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createProduct({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createdProduct = await apiProduct.createProduct(payload);
      commit('setProductDetail', createdProduct);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async updateProduct({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatedProduct = await apiProduct.updateProduct(payload);
      commit('setProductDetail', updatedProduct);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async deleteProduct({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await apiProduct.deleteProduct(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
