/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import axios from 'axios';
import router from '@/router';
import apiGateway from '@/services/jApiGateway';
import apiProfile from '@/services/jApiProfile';
import apiUser from '@/services/jApiUser';
// import Vue from 'vue';

const state = {
  // Set default state(s) here
  idToken: null,
  refreshToken: null,
  profile_data: null,
  currencyFormat: {
    digitGroupSeparator: '.',
    decimalCharacter: ',',
    decimalCharacterAlternative: '.',
    currencySymbol: 'Rp',
    currencySymbolPlacement: 'p',
    roundingMethod: 'U',
    minimumValue: '0',
    maximumValue: '99999999999999',
  },
  is_refresh_token: false,
  login_failed: false,
  data_expire: null,
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  loginFailed(state) {
    state.login_failed = !state.login_failed;
  },

  saveToken(state, data) {
    function parseJwt(token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      return JSON.parse(window.atob(base64));
    }
    state.data_expire = parseJwt(data.token);
    state.idToken = data.token;
    state.refreshToken = data.refresh_token;
    state.is_refresh_token = !state.is_refresh_token;
  },

  setCurrencyFormat(state) {
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },

  setInLocalStorage(state) {
    localStorage.setItem('refreshToken', state.refreshToken);
    const tokenData = {
      value: state.idToken,
      expire: state.data_expire.exp * 1000,
    };
    localStorage.setItem('Token', JSON.stringify(tokenData));
    // Vue.ls.set('Token', state.idToken, 15 * 60 * 1000);
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },

  doLogin() {
    const role = JSON.parse(localStorage.getItem('role'));
    if (role.includes(1) || role.includes(2)) {
      window.location = '/';
    } else if (role.includes(17) || role.includes(18)) {
      router.push('/home');
    }
  },

  setProfile(state, payload) {
    state.profile_data = payload;
    localStorage.setItem('email', state.profile_data.email);
    localStorage.setItem('photo', state.profile_data.photo_url);
    localStorage.setItem('firstName', state.profile_data.first_name);
    localStorage.setItem('lastName', state.profile_data.last_name);
    localStorage.setItem('companyName', '');
    localStorage.setItem('daysRemaining', '15');
  },

  setRole(state, roleData) {
    const role = roleData.data.role.states;
    const roleString = roleData.data.role.names;
    localStorage.setItem('role', JSON.stringify(role));
    localStorage.setItem('roleString', JSON.stringify(roleString));
  },
};

const actions = {
  async loginAccount({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.login(authData);
      commit('saveToken', auth);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      commit('doLogin');
      dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async loginRefreshTokenApi({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.login(authData);
      commit('saveToken', auth);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
      commit('loginFailed');
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async refreshTokenApi({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const newToken = await apiGateway.refreshToken(req);
      commit('saveToken', newToken);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getProfileAndRole({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async refreshTokenApiSingle({ commit, dispatch }, req) {
    try {
      const newToken = await apiGateway.refreshToken(req);
      commit('saveToken', newToken);
      commit('setInLocalStorage');
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
