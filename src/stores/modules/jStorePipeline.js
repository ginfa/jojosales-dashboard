/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiPipeline from '@/services/jApiPipeline';
import apiStage from '@/services/jApiStage';
import apiUserRestriction from '@/services/jApiUserRestriction';
import constant from '@/constant/constant';

const state = {
  // Set default state(s) here
  pipeline_lg_state: {
    stage: [],
    list: [],
    value: 0,
    amount: 0,
  },
  pipeline_sales_state: {
    stage: [],
    list: [],
    value: 0,
    amount: 0,
  },
  user_restriction_id_list: [],
  data_changed: false,
};

const getters = {

};

const mutations = {
  setStageList(state, param) {
    state.pipeline_lg_state.stage = [];
    state.pipeline_sales_state.stage = [];
    param.data.forEach((instance) => {
      const data = {
        id: instance.id,
        status: instance.name,
        summaryDate: '-',
        type: instance.type,
        isStart: (instance.position === 1),
        isEnd: false,
        color: constant.configureColor(instance.color),
        active: (instance.position === 1),
        href: '',
      };
      if (instance.type === constant.StageTypeLG) {
        state.pipeline_lg_state.stage.push(data);
      } else if (instance.type === constant.StageTypeSales) {
        state.pipeline_sales_state.stage.push(data);
      }
    });
    state.pipeline_lg_state.stage[state.pipeline_lg_state.stage.length - 1].isEnd = true;
    state.pipeline_sales_state.stage[state.pipeline_sales_state.stage.length - 1].isEnd = true;
  },
  setUserRestrictionList(state, param) {
    param.data.forEach((data) => {
      state.user_restriction_id_list.push(data.id);
    });
  },
  setPipelineList(state, param) {
    param.data.forEach((instance) => {
      const now = new Date();
      const date = new Date(instance.date);

      let duration = '';
      let d = now.getDate() - date.getDate();

      for (let i = 0; i < now.getMonth() - date.getMonth(); i += 1) {
        const month = new Date(date.getFullYear(), date.getMonth() + i, 0).getDate();
        d += month;
      }

      if (d >= 7) {
        d /= 7;
        d = Math.ceil(d);
        duration = `${d} WEEK${d > 1 ? 'S' : ''}`;
      } else {
        duration = `${d} DAY${d > 1 ? 'S' : ''}`;
      }

      const data = {
        name: instance.name,
        icon: instance.icon,
        user: instance.full_name,
        value: instance.value,
        duration,
      };

      if (param.type === constant.StageTypeLG) {
        state.pipeline_lg_state.value += instance.value;
        state.pipeline_lg_state.amount += 1;
        state.pipeline_lg_state.list.push(data);
      } else if (param.type === constant.StageTypeSales) {
        state.pipeline_sales_state.value += instance.value;
        state.pipeline_sales_state.amount += 1;
        state.pipeline_sales_state.list.push(data);
      }
    });
  },
  configureStage(state, data) {
    if (data.type === constant.StageTypeLG) {
      state.pipeline_lg_state.stage.forEach((stage) => {
        stage.active = false;
      });
      state.pipeline_lg_state.list = [];
      state.pipeline_lg_state.value = 0;
      state.pipeline_lg_state.amount = 0;
    } else if (data.type === constant.StageTypeSales) {
      state.pipeline_sales_state.stage.forEach((stage) => {
        stage.active = false;
      });
      state.pipeline_sales_state.list = [];
      state.pipeline_sales_state.value = 0;
      state.pipeline_sales_state.amount = 0;
    }

    data.active = true;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getStageList({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const stageList = await apiStage.getStageList();
      commit('setStageList', stageList.data);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getUserRestrictionList({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const userRestrictionList = await apiUserRestriction.getUserRestriction(payload);
      commit('setUserRestrictionList', userRestrictionList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getPipelineList({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      commit('configureStage', payload.data);
      const pipelineList = await apiPipeline.getPipeline(payload.payload);
      commit('setPipelineList', { data: pipelineList.data.data, type: payload.data.type });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
