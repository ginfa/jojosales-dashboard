/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteActivityTypes = {
  computed: {
    delete_data() {
      const ids = [];
      if (this.selectedRows.length > 0) {
        this.selectedRows.forEach((row) => {
          const dataRow = {
            ids: [row.id],
          };
          ids.push(dataRow);
        });
        return ids;
      }
      return {
        ids,
      };
    },
  },

  methods: {
    ...mapActions('jStoreActivityType', ['deleteActivityType']),

    deleteActivityTypes() {
      if (this.delete_data !== null) {
        const text = `You want to delete ${this.activity_types_name}`;

        this.$swal({
          title: 'Are you sure?',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            // this.deleteActivityType(this.delete_data);
            this.$refs.deleteActivityType.toggleIsVisible();
          }
        });
      }
    },
  },

};

export const jMixModelDeleteActivityType = {
  computed: {
    ...mapState('jStoreActivityType', ['activity_type_state']),

    delete_data() {
      if (this.activity_type_state.activity_type_detail !== null) {
        return {
          ids: [Number(this.$route.params.idActivityType)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreActivityType', ['deleteActivityType']),

    deleteData() {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this activity type',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.deleteActivityType(this.delete_data);
          }
        });
      }
    },
  },

};
