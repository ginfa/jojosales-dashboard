/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

import { mapState, mapActions } from 'vuex';

export const jMixModeEditGoals = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreGoals', ['getGoalDetail', 'setDefaultDivisionOptions']),
    modeEdit() {
      const goalId = {
        id: Number(this.$route.params.idGoal),
      };

      // get detail
      this.getGoalDetail(goalId);
    },
  },

  computed: {
    ...mapState('jStoreGoals', ['goal_state']),
  },

  watch: {
    'goal_state.goal_detail'(newVal) {
      this.setDefaultDivisionOptions(newVal.division_options);
      const goalData = {
        id: newVal.id,
        name: newVal.name,
        activity_type_id: newVal.activity_type_id,
        type: newVal.type,
        value: newVal.value,
        period_type: newVal.period_type,
        division: newVal.division,
      };
      this.goal_data = goalData;
    },
  },
};
