/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeEditMeasure = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreMeasure', ['getMeasureDetail']),
    modeEdit() {
      const measureId = {
        id: Number(this.$route.params.idMeasure),
      };

      // get detail
      this.getMeasureDetail(measureId);
    },
  },

  computed: {
    ...mapState('jStoreMeasure', ['measure_state']),
  },

  watch: {
    'measure_state.measure_detail'(newVal) {
      const measureData = {
        id: newVal.id,
        name: newVal.name,
        description: newVal.description,
      };
      this.measure_data = measureData;
    },
  },
};
