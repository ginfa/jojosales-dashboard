/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeEditIndustry = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreIndustry', ['getIndustryDetail']),
    modeEdit() {
      const industryId = {
        id: Number(this.$route.params.idIndustry),
      };

      // get detail
      this.getIndustryDetail(industryId);
    },
  },

  computed: {
    ...mapState('jStoreIndustry', ['industry_state']),
  },

  watch: {
    'industry_state.industry_detail'(newVal) {
      const industryData = {
        id: newVal.id,
        name: newVal.name,
      };
      this.industry_data = industryData;
    },
  },
};
