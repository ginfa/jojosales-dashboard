/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeEditReminder = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreReminder', ['getReminderDetail']),
    modeEdit() {
      const reminderId = {
        id: Number(this.$route.params.idReminder),
      };

      // get detail
      this.getReminderDetail(reminderId);
    },
  },

  computed: {
    ...mapState('jStoreReminder', ['reminder_state']),
  },

  watch: {
    'reminder_state.reminder_detail'(newVal) {
      const reminders = [];
      newVal.reminders.forEach((reminder) => {
        reminders.push(reminder);
      });
      const reminderData = {
        id: newVal.id,
        name: newVal.name,
        description: newVal.description,
        reminders,
      };
      this.reminder_data = reminderData;
    },
  },
};
