/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';
import apiReminder from '@/services/jApiReminder';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';

export const jMixPaginationReminder = {
  data() {
    return {
      filter_reminder: '',
      data_table: [],
      selected_rows: [],
      load_first: false,
      columns: ['checkbox', 'name', 'description', 'reminder'],
      options: {
        headings: {
          name: 'NAME',
          description: 'DESCRIPTION',
          reminder: 'REMINDER',
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        filterable: true,
        sortable: ['name'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : 'id',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListProduct = apiReminder.getReminderDataTable(data);
          return getListProduct.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jStoreReminder', ['data_changed']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    ...mapActions('jStoreReminder', ['setDefaultDataChanged']),

    loading() {
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.reminderServer.getData();
    },

    remindersDeleted() {
      this.refreshTable();
      this.selected_rows = [];
    },

    customFilter(val) {
      if (val !== '') {
        this.$refs.reminderServer.setFilter(val);
      }
    },
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
    filter_reminder(val) {
      if (val === '') {
        this.$refs.reminderServer.setFilter('');
      }
    },
    data_changed(val) {
      if (val) {
        this.refreshTable();
        this.setDefaultDataChanged();
        this.$router.replace('/config/activity-type');
      }
    },
  },
};
