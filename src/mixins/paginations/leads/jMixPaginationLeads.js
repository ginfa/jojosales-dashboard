/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing,no-param-reassign */
import { mapActions, mapState } from 'vuex';
import apiLeads from '@/services/jApiLeads';

export const jMixPaginationLeads = {
  data() {
    return {
      filter_leads: '',
      filter_type: 'name',
      filter_options: [
        {
          value: 'name',
          text: 'Name',
        },
        {
          value: 'source',
          text: 'Source',
        },
        {
          value: 'industry',
          text: 'Industry',
        },
      ],
      data_table: [],
      load_first: false,
      columns: ['add_detail', 'name', 'phone_no', 'pic_name', 'pic_phone', 'pic_email', 'leads_source_name', 'industry_name', 'opportunity', 'opportunity_status'],
      options: {
        headings: {
          add_detail: 'MORE DETAILS',
          name: 'COMPANY NAME',
          phone_no: 'OFFICE PHONE',
          pic_name: 'CONTACT PERSON',
          pic_phone: 'PHONE',
          pic_email: 'EMAIL',
          leads_source_name: 'SOURCE',
          industry_name: 'INDUSTRY',
          opportunity: 'OPPORTUNITY',
          opportunity_status: 'OPPORTUNITY STATUS',
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        filterable: true,
        sortable: ['name'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : 'id',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListProduct = apiLeads.getLeadsDataTable(data);
          return getListProduct.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          data.data.forEach((instance) => {
            instance.add_detail = false;
            instance.show_toggle_detail = (instance.pic.length > 1);
            instance.pic_name = [instance.pic[0].name];
            instance.pic_phone = [instance.pic[0].phone_no];
            instance.pic_email = [instance.pic[0].email];
          });
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jStoreLeads', ['data_changed']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    ...mapActions('jStoreLeads', ['setDefaultDataChanged']),

    loading(val) {
      // eslint-disable-next-line
      val.pagination.query_type = this.filter_type;
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.leadsServer.getData();
    },

    customFilter(val) {
      if (val !== '') {
        this.$refs.leadsServer.setFilter(val);
      }
    },
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
    filter_leads(val) {
      if (val === '') {
        this.$refs.leadsServer.setFilter('');
      }
    },
    data_changed(val) {
      if (val) {
        this.$router.replace('/sales/leads');
        this.refreshTable();
        this.setDefaultDataChanged();
      }
    },
  },
};
