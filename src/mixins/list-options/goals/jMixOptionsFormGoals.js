/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing,no-param-reassign */
/* eslint-disable import/no-extraneous-dependencies */

import { ASYNC_SEARCH } from '@riophae/vue-treeselect';
import { mapActions, mapState } from 'vuex';
import constant from '@/constant/constant';
import jApi from '../../../services/jApi';

export const jMixOptionsFormGoals = {
  created() {
    this.optionsGeneral();
  },

  computed: {
    ...mapState('jStoreGoals', ['goal_state']),
    goal_type_options() {
      return [
        {
          name: 'Value',
          id: constant.GoalTypeValue,
        },
        {
          name: 'Amount',
          id: constant.GoalTypeNumber,
        },
      ];
    },
    period_type_options() {
      return [
        {
          name: 'Daily',
          id: constant.PeriodTypeDaily,
        },
        {
          name: 'Weekly',
          id: constant.PeriodTypeWeekly,
        },
        {
          name: 'Monthly',
          id: constant.PeriodTypeMonthly,
        },
        {
          name: 'Yearly',
          id: constant.PeriodTypeYearly,
        },
      ];
    },
  },

  methods: {
    ...mapActions('jStoreGoals', ['getParameterOptions', 'setDefaultDivisionOptions']),
    optionsGeneral() {
      this.getParameterOptions();
      this.setDefaultDivisionOptions([]);
      // options which use in mode create and edit.
    },

    loadDivisionOptions({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 0,
            page: 0,
            column: 'name',
            ascending: true,
            query: searchQuery,
          },
        };
        this.getListDivision(reqData, callback);
      }
    },

    getListDivision(req, callback) {
      const api = jApi.generateApi();
      return api.post('jojoflow/setup/division/list', req)
        .then((res) => {
          if (typeof res.data.data !== 'undefined') {
            res.data.data.forEach((lab, index) => {
              res.data.data[index].label = lab.name;
            });
            callback(null, res.data.data);
          }
          return res.data;
        });
    },
  },
};
