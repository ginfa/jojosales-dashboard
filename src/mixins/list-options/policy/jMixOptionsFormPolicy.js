import constant from '@/constant/constant';

/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

export const jMixOptionsFormPolicy = {
  created() {
    this.optionsGeneral();
  },

  computed: {
    documentOptions() {
      return [
        {
          text: 'Document Template',
          value: 0,
        },
      ];
    },
    periodType() {
      return [
        {
          text: 'Weeks',
          value: constant.PeriodTypeWeeks,
        },
        {
          text: 'Months',
          value: constant.PeriodTypeMonths,
        },
        {
          text: 'Years',
          value: constant.PeriodTypeYears,
        },
      ];
    },
  },

  methods: {
    optionsGeneral() {
      // options which use in mode create and edit.
    },
  },

};
