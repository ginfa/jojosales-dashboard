import constant from '@/constant/constant';

/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

export const jMixOptionsFormReminder = {
  created() {
    this.optionsGeneral();
    if (this.mode === 'create') {
      this.optionsCreate();
    } else if (this.mode === 'edit') {
      this.optionsEdit();
    }
  },
  computed: {
    reminder_options() {
      return [
        {
          name: 'Reminder',
          id: 1,
        },
        // {
        //   text: 'Notification',
        //   value: constant.ReminderTypeNotification,
        // },
      ];
    },
    period_type_options() {
      return [
        // {
        //   text: 'Period',
        //   value: 2,
        // },
        {
          name: 'Minutes',
          id: constant.PeriodTypeMinutes,
        },
        {
          name: 'Hours',
          id: constant.PeriodTypeHours,
        },
        {
          name: 'Days',
          id: constant.PeriodTypeDays,
        },
        {
          name: 'Weeks',
          id: constant.PeriodTypeWeeks,
        },
        {
          name: 'Months',
          id: constant.PeriodTypeMonths,
        },
      ];
    },
  },
  methods: {
    optionsGeneral() {
      // options which use in mode create and edit.
    },

    optionsCreate() {
      // options which use in mode create only.
    },

    optionsEdit() {
      // options which use in mode edit only.
    },
  },

};
