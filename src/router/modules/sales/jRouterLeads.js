import jGlobalLeads from '@/views/sales/leads/jGlobalLeads';
import jCreateLeads from '@/views/sales/leads/create-leads/jCreateLeads';
import jDetailLeads from '@/views/sales/leads/detail-leads/jDetailLeads';
import auth from '@/middleware/auth';

export default [
  {
    path: '/sales/leads',
    name: 'jLeads',
    component: jGlobalLeads,
    meta: {
      title: 'Leads',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/sales/leads/create',
        name: 'jCreateLeads',
        component: jCreateLeads,
        meta: {
          title: 'Create Leads',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/sales/leads/detail/:idLeads',
        name: 'jDetailLeads',
        component: jDetailLeads,
        meta: {
          title: 'Detail Leads',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
    ],
  },
];
