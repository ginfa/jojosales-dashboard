import jGlobalPipeline from '@/views/sales/pipeline/jGlobalPipeline';
import auth from '@/middleware/auth';

export default [
  {
    path: '/sales/pipeline',
    name: 'jRouterPipeline',
    component: jGlobalPipeline,
    meta: {
      title: 'Pipeline',
      middleware: [auth],
      layout: 'jWrapper',
    },
  },
];
