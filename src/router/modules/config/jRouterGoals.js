import jGoals from '@/views/config/goals/jGoals';
import jCreateGoals from '@/views/config/goals/create-goals/jCreateGoals';
import jEditGoals from '@/views/config/goals/edit-goals/jEditGoals';
import auth from '@/middleware/auth';

export default [
  {
    path: '/config/goals',
    name: 'jGoals',
    component: jGoals,
    meta: {
      title: 'Goals',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/config/goals/create',
        name: 'jCreateGoals',
        component: jCreateGoals,
        meta: {
          title: 'Create Goals',
          middleware: [auth],
        },
      },
      {
        path: '/config/goals/edit/:idGoal',
        name: 'jEditGoals',
        component: jEditGoals,
        meta: {
          title: 'Edit Goals',
          middleware: [auth],
        },
      },
    ],
  },
];
