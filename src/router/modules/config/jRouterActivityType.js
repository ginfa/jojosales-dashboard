import jGlobalActivityType from '@/views/config/activity-type/jGlobalActivityType';
import jCreateActivityType from '@/views/config/activity-type/main/create-activity-type/jCreateActivityType';
import jEditActivityType from '@/views/config/activity-type/main/edit-activity-type/jEditActivityType';
import jCreatePolicy from '@/views/config/activity-type/policy/create-policy/jCreatePolicy';
import jEditPolicy from '@/views/config/activity-type/policy/edit-policy/jEditPolicy';
import jCreateReminder from '@/views/config/activity-type/reminder/create-reminder/jCreateReminder';
import jEditReminder from '@/views/config/activity-type/reminder/edit-reminder/jEditReminder';
import auth from '@/middleware/auth';

export default [
  {
    path: '/config/activity-type',
    name: 'jGlobalActivityType',
    component: jGlobalActivityType,
    meta: {
      title: 'Activity Type Configuration',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/config/activity-type/create',
        name: 'jCreateActivityType',
        component: jCreateActivityType,
        meta: {
          title: 'Create Activity Type',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/activity-type/edit/:idActivityType',
        name: 'jEditActivityType',
        component: jEditActivityType,
        meta: {
          title: 'Edit Activity Type',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/activity-type/policy/create',
        name: 'jCreatePolicy',
        component: jCreatePolicy,
        meta: {
          title: 'Create Policy',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/activity-type/policy/edit/:idPolicy',
        name: 'jEditPolicy',
        component: jEditPolicy,
        meta: {
          title: 'Edit Policy',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/activity-type/reminder/create',
        name: 'jCreateReminder',
        component: jCreateReminder,
        meta: {
          title: 'Create Reminder',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/activity-type/reminder/edit/:idReminder',
        name: 'jEditReminder',
        component: jEditReminder,
        meta: {
          title: 'Edit Reminder',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
    ],
  },
];
