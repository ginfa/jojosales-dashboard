import jGlobalReport from '@/views/report/jGlobalReport';
import auth from '@/middleware/auth';

export default [
  {
    path: '/report',
    name: 'jGlobalReport',
    component: jGlobalReport,
    meta: {
      title: 'Report',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      // {
      //   path: '/config/source/create',
      //   name: 'jCreateSource',
      //   component: jCreateSource,
      //   meta: {
      //     title: 'Create Source',
      //     middleware: [auth],
      //     layout: 'jWrapper',
      //   },
      // },
      // {
      //   path: '/config/source/edit/:idSource',
      //   name: 'jEditSource',
      //   component: jEditSource,
      //   meta: {
      //     title: 'Edit Source',
      //     middleware: [auth],
      //     layout: 'jWrapper',
      //   },
      // },
    ],
  },
];
