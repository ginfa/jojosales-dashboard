import jApi from './jApi';

export default {
  getMyCalendar(req) {
    const api = jApi.generateApi();
    return api.post('sales/activity/my-calendar', req)
      .then(res => res);
  },

  getMyTeam(req) {
    const api = jApi.generateApi();
    return api.post('sales/my-team/calendar', req)
      .then(res => res);
  },

};
