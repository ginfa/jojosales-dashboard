import jApi from './jApi';

export default {
  getReportActivity(req) {
    const api = jApi.generateApi();
    return api.post('sales/report/activity', req)
      .then(res => res);
  },

  getReportOpportunity(req) {
    const api = jApi.generateApi();
    return api.post('sales/report/opportunity', req)
      .then(res => res);
  },

  getReportLead(req) {
    const api = jApi.generateApi();
    return api.post('sales/report/leads', req)
      .then(res => res);
  },
};
