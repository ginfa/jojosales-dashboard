import jApi from './jApi';

export default {
  getUserRestriction(req) {
    const api = jApi.generateApi();
    return api.post('user-restriction/user-list', req)
      .then(res => res);
  },
};
